package clock;
import java.awt.Color;
import java.awt.Graphics;

/**
 * A Digit that can be drawn at the specified location. it is drawn to look like a typical 7-segment display.
 * 
 * @author Chuck Cusack, 2001. Extensively refactored, January 2013.
 */
public class Digit {
	private int	x;
	private int	y;
	private int	digit;

	/**
	 * The digit value to be drawn with upper-left corner at (x,y). Value may be 0-10, with 0-9 representing the digits
	 * 0-9 (as expected) and 10 representing a colon. A value larger than 10 will be replaced with 0.
	 * 
	 * @param x
	 * @param y
	 * @param value the value to be displayed.
	 */
	public Digit(int x, int y, int value) {
		this.x = x;
		this.y = y;
		setDigit(value);
	}

	public int getDigit() {
		return digit;
	}

	/**
	 * Set the digit to be displayed, replacing any value above 10 with 0.
	 * 
	 * @param value the value to be displayed.
	 */
	public void setDigit(int value) {
		if (value > 10) {
			this.digit = 0;
		} else {
			this.digit = value;
		}
	}

	/**
	 * Draw the digit on the Graphics object with upper-left corner at (x,y).
	 * 
	 * @param g The Graphics object to draw the digit on.
	 */
	public void drawDigit(Graphics g) {
		g.translate(x, y);
		switch (digit) {
			case 0:
				drawZero(g);
				break;
			case 1:
				drawOne(g);
				break;
			case 2:
				drawTwo(g);
				break;
			case 3:
				drawThree(g);
				break;
			case 4:
				drawFour(g);
				break;
			case 5:
				drawFive(g);
				break;
			case 6:
				drawSix(g);
				break;
			case 7:
				drawSeven(g);
				break;
			case 8:
				drawEight(g);
				break;
			case 9:
				drawNine(g);
				break;
			case 10:
				drawColon(g);
				break;
			default:
				break;
		}
		g.translate(-x, -y);
	}

	/*
	 * Private helper methods.
	 */
	private void drawZero(Graphics g) {
		drawLT(g);
		drawLB(g);
		drawRT(g);
		drawRB(g);
		drawBL(g);
		drawTL(g);
	}

	private void drawOne(Graphics g) {
		drawRT(g);
		drawRB(g);
	}

	private void drawTwo(Graphics g) {
		drawLB(g);
		drawRT(g);
		drawBL(g);
		drawTL(g);
		drawML(g);
	}

	private void drawThree(Graphics g) {
		drawRT(g);
		drawRB(g);
		drawBL(g);
		drawTL(g);
		drawML(g);
	}

	private void drawFour(Graphics g) {
		drawLT(g);
		drawRT(g);
		drawRB(g);
		drawML(g);
	}

	private void drawFive(Graphics g) {
		drawLT(g);
		drawRB(g);
		drawBL(g);
		drawTL(g);
		drawML(g);
	}

	private void drawSix(Graphics g) {
		drawLT(g);
		drawLB(g);
		drawRB(g);
		drawBL(g);
		drawTL(g);
		drawML(g);
	}

	private void drawSeven(Graphics g) {
		drawRT(g);
		drawRB(g);
		drawTL(g);
	}

	private void drawEight(Graphics g) {
		drawLT(g);
		drawLB(g);
		drawRT(g);
		drawRB(g);
		drawBL(g);
		drawTL(g);
		drawML(g);
	}

	private void drawNine(Graphics g) {
		drawLT(g);
		drawRT(g);
		drawRB(g);
		drawTL(g);
		drawML(g);
	}

	private void drawColon(Graphics g) {
		drawTD(g);
		drawBD(g);
	}

	private void drawLT(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(0, 5, 6, 20, 10, 10);
	}

	private void drawML(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(7, 22, 21, 6, 10, 10);
	}

	private void drawTL(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(7, 0, 22, 6, 10, 10);
	}

	private void drawBL(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(7, 45, 22, 6, 10, 10);
	}

	private void drawRB(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(30, 27, 6, 19, 10, 10);
	}

	private void drawRT(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(30, 5, 6, 20, 10, 10);
	}

	private void drawLB(Graphics g) {
		g.setColor(Color.green);
		g.fillRoundRect(0, 27, 6, 19, 10, 10);
	}

	private void drawTD(Graphics g) {
		g.setColor(Color.blue);
		g.fillOval(13, 15, 8, 8);
	}

	private void drawBD(Graphics g) {
		g.setColor(Color.blue);
		g.fillOval(13, 30, 8, 8);
	}
}
